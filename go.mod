module gitlab.com/automac/chart

go 1.13

require (
	github.com/fatih/color v1.7.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/spf13/cobra v0.0.5
	gitlab.com/automac/cmdhelper v0.0.0-20190502145440-cb52ddfbcacd
	golang.org/x/net v0.0.0-20191014212845-da9a3fd4c582 // indirect
	gonum.org/v1/netlib v0.0.0-20190926062253-2d6e29b73a19 // indirect
	gonum.org/v1/plot v0.0.0-20191004082913-159cd04f920c
)
