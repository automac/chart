// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"image/color"
	"math"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/automac/cmdhelper"
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"gonum.org/v1/plot/vg/draw"
	"gonum.org/v1/plot/vg/vgsvg"
)

var cfgFile string

var (
	version bool
)

var Version = "dev"

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "chart",
	Short: "A simple program to generate svg charts",
	Long: cmdhelper.MustFormatString(`
<p>
	chart is a simple program that takes a json object piped into stdin and
	generates an SVG chart. The input is in the following format:
</p>
<pre>
{
    "title": "Chart Title",
    "height": "4in",
    "width": "10cm",
    "x_axis": {
        "label": "X Axis Title"
        "min": 0,
        "max": 10
    },
    "y_axis": {
        "label": "Y Axis Title"
        "min": -5,
        "max": 5
    },
    "data": [
        {
            "color": "#ff5555",
            "points": [
                { "x": 1.5, "y": 10.982 },
                ...
            ]
        },
        ...
    ],
    "bands": [
        {
            "title": "test",
            "start": 3,
            "end": 4.5,
            "color": "#00ff00"
        },
        ...
    ]
}
</pre>
`),
	Run: func(cmd *cobra.Command, args []string) {
		if version {
			fmt.Printf("chart version %s\n", Version)
			return
		}
		rand.Seed(int64(2))

		o := &Options{
			Width:  "4in",
			Height: "4in",
		}

		check(json.NewDecoder(os.Stdin).Decode(o), "failed to read json")

		p, err := plot.New()
		check(err, "failed initialize plot")

		p.Title.Text = o.Title

		check(updateAxis(&p.X, o.XAxis), "failed to generate x axis")
		check(updateAxis(&p.Y, o.YAxis), "failed to generate y axis")

		for _, band := range o.Bands {
			p.Add(band)
		}
		for _, series := range o.Data {
			for _, section := range series.sections() {
				check(addSeries(p, section), "failed to add series")
			}
		}

		height, err := vg.ParseLength(o.Height)
		check(err, "invalid height %s", o.Height)
		width, err := vg.ParseLength(o.Width)
		check(err, "invalid width %s", o.Width)

		c := vgsvg.New(width, height)
		p.Draw(draw.New(c))
		_, err = c.WriteTo(os.Stdout)
		check(err, "failed to write to stdout")
	},
}

func updateAxis(pa *plot.Axis, a Axis) error {
	pa.Label.Text = a.Label
	if a.Min != nil {
		pa.Min = *a.Min
	}
	if a.Max != nil {
		pa.Max = *a.Max
	}

	if a.Time {
		tz := time.UTC
		if a.TimeZone != "" {
			var err error
			tz, err = time.LoadLocation(a.TimeZone)
			if err != nil {
				return err
			}
		}
		pa.Tick.Marker = plot.TimeTicks{
			Format: time.Kitchen,
			Time:   plot.UnixTimeIn(tz),
		}
	}
	return nil
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().BoolVarP(&version, "version", "v", false, "display the version of chart")
}

type Axis struct {
	Label    string   `json:"label"`
	Min      *float64 `json:"min"`
	Max      *float64 `json:"max"`
	Time     bool     `json:"time"`
	TimeZone string   `json:"time_zone"`
}

type Point struct {
	X float64  `json:"x"`
	Y *float64 `json:"y"`
}

type Series struct {
	Points           []Point `json:"points"`
	Color            Color   `json:"color"`
	HidePointMarkers bool    `json:"hide_point_markers"`
}

func (s *Series) sections() []*Series {
	sections := []*Series{}
	var currentSection *Series
	for _, point := range s.Points {
		if point.Y == nil {
			if currentSection != nil {
				sections = append(sections, currentSection)
				currentSection = nil
			}
			continue
		}

		if currentSection == nil {
			currentSection = &Series{
				Points:           []Point{},
				Color:            s.Color,
				HidePointMarkers: s.HidePointMarkers,
			}
		}
		currentSection.Points = append(currentSection.Points, point)
	}
	if currentSection != nil {
		sections = append(sections, currentSection)
	}
	return sections
}

func (s *Series) Len() int {
	return len(s.Points)
}

func (s *Series) XY(i int) (x, y float64) {
	point := s.Points[i]
	if point.Y == nil {
		return point.X, 0
	}
	return point.X, *point.Y
}

type Options struct {
	Title  string    `json:"title"`
	XAxis  Axis      `json:"x_axis"`
	YAxis  Axis      `json:"y_axis"`
	Height string    `json:"height"`
	Width  string    `json:"width"`
	Data   []*Series `json:"data"`
	Bands  []*Band   `json:"bands"`
}

type Color string

func (c Color) RGBA() (r, g, b, a uint32) {

	matches := regexp.
		MustCompile(`^#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$`).
		FindStringSubmatch(string(c))

	if len(matches) == 0 {
		return 0, 0, 0, math.MaxUint32
	}

	parts := []uint32{}

	for _, hex := range matches[1:] {
		dec, err := strconv.ParseInt(hex, 16, 32)
		if err != nil {
			panic(err)
		}
		parts = append(parts, uint32(dec*math.MaxUint32/255))
	}
	return parts[0], parts[1], parts[2], math.MaxUint32
}

// https://github.com/gonum/plot/wiki/Example-plots

func check(err error, f string, a ...interface{}) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "chart error: %s: %v\n", fmt.Sprintf(f, a...), err)
		os.Exit(1)
	}
}

func warn(str ...interface{}) {
	fmt.Fprintln(os.Stderr, str...)
}
func warnf(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
}

func addSeries(p *plot.Plot, series *Series) error {
	line, points, err := plotter.NewLinePoints(series)
	if err != nil {
		return err
	}
	line.Color = series.Color
	points.Color = series.Color
	p.Add(line)
	if !series.HidePointMarkers {
		p.Add(points)
	}
	return nil
}

type Band struct {
	Title string  `json:"title"`
	Start float64 `json:"start"`
	End   float64 `json:"end"`
	Color Color   `json:"color"`
}

func (b *Band) Plot(c draw.Canvas, plt *plot.Plot) {
	trX, _ := plt.Transforms(&c)

	bottomLeft := vg.Point{X: trX(b.Start), Y: c.Min.Y}
	topLeft := vg.Point{X: trX(b.Start), Y: c.Max.Y}
	topRight := vg.Point{X: trX(b.End), Y: c.Max.Y}
	bottomRight := vg.Point{X: trX(b.End), Y: c.Min.Y}

	c.FillPolygon(b.Color, []vg.Point{
		bottomLeft,
		topLeft,
		topRight,
		bottomRight,
	})
	fnt, err := vg.MakeFont(plot.DefaultFont, vg.Points(11))
	if err != nil {
		panic(err)
	}
	c.FillText(draw.TextStyle{
		Color:    contrastColor(b.Color),
		Font:     fnt,
		Rotation: -math.Pi / 2,
	}, topLeft.Add(vg.Point{X: vg.Points(4), Y: -vg.Points(4)}), b.Title)
}

func contrastColor(c color.Color) color.Color {
	r, g, b, _ := c.RGBA()
	a := 1 - ((0.2126*float64(r) + 0.7152*float64(g) + 0.0722*float64(b)) / math.MaxUint32)

	if a < 0.5 {
		return color.Black
	} else {
		return color.White
	}
}
